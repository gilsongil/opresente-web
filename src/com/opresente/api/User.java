package com.opresente.api;

import java.io.Serializable;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int identifier;
	private String name;
	private String email;
	private String token;
	
	public User(int identifier, 
			String name, 
			String email, 
			String token) {
		super();
		this.identifier = identifier;
	    this.name = name;
	    this.email = email;
	    this.token = token;
	  }
	
	public User(){}
	
	  public int getIdentifier() {
	    return identifier;
	  }

	  public void setIdentifier(int identifier) {
	    this.identifier = identifier;
	  }
	  
	  public String getName() {
	    return name;
	  }
	  public void setName(String name) {
	    this.name = name;
	  }
	  
	  public String getEmail() {
	    return email;
	  }
	  public void setEmail(String email) {
	    this.email = email;
	  }
	  
	  public String getToken() {
	    return token;
	  }
	  public void setToken(String token) {
	    this.token = token;
	  }

	  @Override
	  public String toString() {
	    return "User [identifier=" 
	              + identifier 
	              + ", name=" 
	              + name 
	              + ", email=" 
	              + email
	              + ", token=" 
	              + token
	              + "]";
	  }	
	  
	  public static User mock() {
		  User user = new User();
		  
		  user.identifier = 1;
		  user.name = "Gilson Gil";
		  user.email = "gilson.gil@me.com";
		  user.token = "";
		  
		  return user;
	  }
}