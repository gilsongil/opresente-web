package com.opresente.api;

import java.io.Serializable;
import java.util.ArrayList;

public class BDay implements Serializable {
	private static final long serialVersionUID = 1L;

	private int identifier;
	private String name;
	private String description;
	private String date;
	private ArrayList<Gift> gifts;
	private User user;
	private String beaconTag;
	private int beaconMinor;
	private int beaconMajor;

	public BDay(int identifier, 
			String name, 
			String description, 
			String date, 
			ArrayList<Gift> gifts, 
			User user, 
			String beaconTag, 
			int beaconMinor, 
			int beaconMajor) {
		super();
		this.identifier = identifier;
	    this.name = name;
	    this.description = description;
	    this.date = date;
	    this.gifts = gifts;
	    this.user = user;
	    this.beaconTag = beaconTag;
	    this.beaconMinor = beaconMinor;
	    this.beaconMajor = beaconMajor;
	  }
	
	public BDay(){}
	
	  public int getIdentifier() {
	    return identifier;
	  }

	  public void setIdentifier(int identifier) {
	    this.identifier = identifier;
	  }
	  
	  public String getName() {
	    return name;
	  }
	  public void setName(String name) {
	    this.name = name;
	  }
	  
	  public String getDescription() {
	    return description;
	  }
	  public void setDescription(String description) {
	    this.description = description;
	  }
	  
	  public String getDate() {
	    return date;
	  }
	  public void setDate(String date) {
	    this.date = date;
	  }
	  
	  public ArrayList<Gift> getGifts() {
	    return gifts;
	  }
	  public void setGifts(ArrayList<Gift> gifts) {
	    this.gifts = gifts;
	  }
	  
	  public User getUser() {
	    return user;
	  }
	  public void setUser(User user) {
	    this.user = user;
	  }
	  
	  public String getBeaconTag() {
	    return beaconTag;
	  }
	  public void setBeaconTag(String beaconTag) {
	    this.beaconTag = beaconTag;
	  }
	  
	  public int getBeaconMinor() {
	    return beaconMinor;
	  }
	  public void setBeaconMinor(int beaconMinor) {
	    this.beaconMinor = beaconMinor;
	  }
	  
	  public int getBeaconMajor() {
	    return beaconMajor;
	  }
	  public void setBeaconMajor(int beaconMajor) {
	    this.beaconMajor = beaconMajor;
	  }

	  @Override
	  public String toString() {
	    return "BDay [identifier=" 
	              + identifier 
	              + ", name=" 
	              + name 
	              + ", description=" 
	              + description
	              + "]";
	  }
	  
	  public static BDay getWithId(String id) {
		  BDay bday = new BDay();
		  if (id.equals("BDAYGILSONGIL86")) {
			  bday.name = "Aniversário do Gil";
			  bday.description = "Todos os anos Gil ganha muitos presentes, este ano gostaríamos de proporcionar uma viagem que marcará a vida dele e pedimos a ajuda de vocês para realizar este sonho.";
			  bday.date = "27 de Outubro de 2018";
			  bday.gifts = Gift.mockGifts();
			  bday.user = User.mock();
			  bday.beaconTag = "E2C56DB5-DFFB-48D2-B060-D0F5A71096E0";
			  bday.beaconMinor = 5324;
			  bday.beaconMajor = 7682;
		  } else if (id.equals("BDAYTESTE")) {
			  bday.name = "Evento teste";
			  bday.description = "Evento teste";
			  bday.date = "30 de Outubro de 2018";
			  bday.gifts = Gift.mockGifts();
			  bday.user = User.mock();
			  bday.beaconTag = "74278BDA-B644-4520-8F0C-720EAF059935";
			  bday.beaconMinor = 1637;
			  bday.beaconMajor = 1273;
		  } else {
			  bday = null;
		  }
		  return bday;
	  }
}