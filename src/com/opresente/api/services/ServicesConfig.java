package com.opresente.api.services;

import org.glassfish.jersey.server.ResourceConfig;

public class ServicesConfig extends ResourceConfig  {
	public ServicesConfig() {
	  packages("com.opresente.api.services");
  	}
}