package com.opresente.api.services;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import com.opresente.api.BDay;

@Path("/")
public class BDayService {

//  @GET
//  @Path("/bday")
//  @Produces("text/plain")
//  public String testaServico() {
//    return "Olá, o serviço funciona";
//  }	
  
  @GET
  @Path("/bday")
  @Produces({MediaType.APPLICATION_JSON})
  public BDay getBDayById(@QueryParam("id") String id) {
    try {		
      BDay bday = BDay.getWithId(id);
      if (bday == null) {
    	  throw new WebApplicationException(Status.NOT_FOUND);
      } else {
    	  return bday;
      }
    } catch (EntityNotFoundException e) {
      throw new WebApplicationException(Status.NOT_FOUND);
    } catch (Exception e) {
      throw new WebApplicationException(
        Status.INTERNAL_SERVER_ERROR);
      }
   }
}