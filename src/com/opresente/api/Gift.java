package com.opresente.api;

import java.io.Serializable;
import java.util.ArrayList;

public class Gift implements Serializable {
	private static final long serialVersionUID = 1L;

	private String identifier;
	private String title;
	private String description;
	private float value;

	public Gift(String identifier, 
			String title, 
			String description, 
			float value) {
		super();
		this.identifier = identifier;
	    this.title = title;
	    this.description = description;
	    this.value = value;
	  }
	
	public Gift(){}
	
	  public String getIdentifier() {
	    return identifier;
	  }

	  public void setIdentifier(String identifier) {
	    this.identifier = identifier;
	  }
	  
	  public String getTitle() {
	    return title;
	  }
	  public void setTitle(String title) {
	    this.title = title;
	  }
	  
	  public String getDescription() {
	    return description;
	  }
	  public void setDescription(String description) {
	    this.description = description;
	  }
	  
	  public float getValue() {
	    return value;
	  }
	  public void setValue(float value) {
	    this.value = value;
	  }

	  @Override
	  public String toString() {
	    return "Gift [identifier=" 
	              + identifier 
	              + ", title=" 
	              + title 
	              + ", description=" 
	              + description
	              + ", value=" 
	              + value
	              + "]";
	  }	
	  
	  public static ArrayList<Gift> mockGifts() {
		  ArrayList<Gift> gifts = new ArrayList<Gift>();
		  
		  Gift gift1 = new Gift();
		  gift1.identifier = "81g237h9";
		  gift1.title = "Hospedagem na Disney";
		  gift1.description = "1 diária no hotel da disney";
		  gift1.value = 30;
		  gifts.add(gift1);

		  Gift gift2 = new Gift();
		  gift2.identifier = "y8193hu4";
		  gift2.title = "Cota de passagem para Orlando/EUA";
		  gift2.description = "Cota no valor de 50 reais para passagem para Orlando/EUA";
		  gift2.value = 50;
		  gifts.add(gift2);
		  
		  return gifts;
	  }
}